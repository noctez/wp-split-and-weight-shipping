<?php

/**
 * Plugin Name: ZWC Split and Weight Based Shipping
 * Description: Split shipping packages based on class and get price by weight
 * Version: 1.0.1
 * Author: Vasyl Halushchak @ Grafica Isernina
 * License: Private
 * WC tested up to: 4.5
 */

### NOTES
# https://gist.github.com/mikejolley/347b8f162257f6736c4d

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}

add_filter( 'woocommerce_cart_shipping_packages', function ($packages){
	// Reset all packages
	$packages              = [];
	$split_package_items   = [];

	foreach ( WC()->cart->get_cart() as $item_key => $item ) {

		if ( $item['data']->needs_shipping() ) {
			$split_package_items[ $item['data']->get_shipping_class() ][] = $item;
		}

	}

	if ( $split_package_items ) {
		foreach ($split_package_items as $package) {

			$packages[] = [
				'contents'        => $package,
				'contents_cost'   => array_sum( wp_list_pluck( $package, 'line_total' ) ),
				'applied_coupons' => WC()->cart->get_applied_coupons(),
				'user'            => [
					'ID' => get_current_user_id(),
				],
				'destination'  => [
					'country'   => WC()->customer->get_shipping_country(),
					'state'     => WC()->customer->get_shipping_state(),
					'postcode'  => WC()->customer->get_shipping_postcode(),
					'city'      => WC()->customer->get_shipping_city(),
					'address'   => WC()->customer->get_shipping_address(),
					'address_2' => WC()->customer->get_shipping_address_2()
				]
			];
		}
	}

	return $packages;
});

// Force cache bust on ajax event
add_action('woocommerce_checkout_update_order_review', function( $post_data ) {
	$packages = WC()->cart->get_shipping_packages();
	foreach ($packages as $package_key => $package ) {
		WC()->session->set( 'shipping_for_package_' . $package_key, false ); // Or true
	}
}, 10, 1);


// Always choose the cheapest option
// add_filter( 'woocommerce_shipping_chosen_method', function(){
// 	$the_cheapest_cost = 1000000;
// 	$packages = WC()->shipping()->get_packages()[0]['rates'];

// 	foreach ( array_keys( $packages ) as $key ) {
// 		if ( ( $packages[$key]->cost > 0 ) && ( $packages[$key]->cost < $the_cheapest_cost ) ) {
// 				$the_cheapest_cost = $packages[$key]->cost;
// 				$method_id = $packages[$key]->id;
// 		}
// 	}

// 	return $method_id;

// }, 10 );

// REMOVE SHIPPING LABEL FROM ORDER TABLE
add_filter( 'woocommerce_cart_shipping_method_full_label', function($label, $method) {
	$new_label = preg_replace( '/^.+:/', '', $label );
	return $new_label;
}, 10, 2 );


add_filter( 'woocommerce_shipping_methods', function ( $methods ) {
	// $method contains available shipping methods
	$methods[ 'z_ship' ] = 'WC_Shipping_Z';
	return $methods;
});

add_action('woocommerce_shipping_init', function(){
	class WC_Shipping_Z extends \WC_Shipping_Method {

		/**
		 * Constructor. The instance ID is passed to this.
		 */
		public function __construct( $instance_id = 0 ) {
			$this->id                 = 'z_ship';
			$this->instance_id        = absint( $instance_id );
			$this->method_title       = __( 'GLS' );
			$this->method_description = '';
			$this->supports           = [
				'shipping-zones',
				'instance-settings',
			];

			$this->instance_form_fields = [
				'enabled' => [
					'title' 		=> __( 'Enable/Disable' ),
					'type' 		=> 'checkbox',
					'label' 		=> __( 'Attiva metodo di spedizione' ),
					'default' 	=> 'yes',
				],
				'title' => [
					'title'        => __( 'Nome' ),
					'type' 			=> 'text',
					'description' 	=> __( 'Nome metodo di spedizione' ),
					'default'		=> __( 'GLS' ),
					'desc_tip'		=> true
				],
				'discount' => [
					'title'        => __( 'Sconto (%)' ),
					'type' 			=> 'number',
					'description' 	=> __( 'Imposta uno sconto in percentuale' ),
					'default'		=> 0,
					'desc_tip'		=> true
				],
				'rates' => [
					'title'        => __( 'Tabella costi' ),
					'type' 			=> 'textarea',
					'description' 	=> __( 'Tabella prezzi (Nome|Peso Max|Costo)' ),
					'default'		=> 'Gratuita|0.01|0
3kg|3|10
10Kg|10|15
20kg|20|18
30kg|30|20
50kg|50|25
75kg|75|38
100kg|100|50
150kg|150|66
200kg|200|90
300kg|300|110
400kg|400|150
500kg|500|170
600kg|600|250
700kg|700|290
800kg|800|330
900kg|900|380
1000kg|1000|420
oltre 1000kg|100000|500', // do not indent
					'desc_tip'		=> true
				]
			];

			$this->enabled = $this->get_option( 'enabled' );
			$this->title   = $this->get_option( 'title' );

			add_action( 'woocommerce_update_options_shipping_' . $this->id, [ $this, 'process_admin_options' ] );
		}

		public function calculate_shipping( $package = [] ) {

			$rates = [];
			foreach (explode(PHP_EOL, $this->get_option( 'rates' )) as $row) {
				$newrate = [];
				$ex_row = explode('|', $row);

				if (sizeof($ex_row) === 3) {
					$newrate = [
						"title" => strval($ex_row[0]),
						"maxwt" => floatval($ex_row[1]),
						"cost"  => floatval($ex_row[2]),
					];
				}

				$rates[] = $newrate;
			}

			$products_weight = 0;
			foreach ($package['contents'] as $product) {
				$products_weight += floatval($product['data']->get_weight());
			}

			$best_rate = [
				"title" => 'oltre 1000kg',
				"maxwt" => 100000,
				"cost" => 500
			];

			foreach (array_reverse($rates) as $rate) {
				if ( $products_weight < $rate['maxwt'] ) {
					$best_rate = $rate;
				}
			}
			$this->instance_id = hash('crc32', json_encode($best_rate), FALSE);
			$this->title = $best_rate['title'];

			$discount = floatval($this->get_option( 'discount' ));

			$this->add_rate( [
				'id'      => $this->id . ":" . $this->instance_id,
				'label'   => $this->title,
				// 'package' => $package,
				'cost'    => $best_rate['cost'] - (floatval($best_rate['cost']) / 100) * $discount,
			] );

			if ($discount > 0) {
				add_filter('woocommerce_cart_shipping_method_full_label',function($a) use($best_rate){
					return $a . '<span class="strike" style="margin-left: 10px; opacity: .7"> ' . wc_price($best_rate['cost']) . '</span>';
				});
			}
		}
	}
});